<?php

use Deskpro\Calc\Calculator;

class CalcTest extends \PHPUnit_Framework_TestCase {

    public function testCorrectOutput()
    {
        $calc = new Calculator();
        $this->assertEquals(7, $calc->evaluate('1 + 1 * 3 + 3'));
        $this->assertEquals(15, $calc->evaluate('10 + 5 + 10 * 5 / 10 - 5'));
    }

}
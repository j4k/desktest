<?php namespace Deskpro\Calc;

class Calculator {

    public function evaluate($expression)
    {
        $expression = str_replace('- ', '+ -', $expression);
        $expression = explode('+', trim($expression));

        return array_reduce($expression, function($current, $next){
            $current += $this->resolve($next);
            return $current;
        }, 0);
    }

    protected function resolve($part)
    {
        $total = 0;
        $part = explode(' ', trim($part));

        if(count($part) == 1)
            return array_shift($part);

        $inc = 0;
        // reset keys after array_filter
        $operands = array_values(array_filter($part, 'is_numeric'));
        $operators = array_values(array_filter($part, [$this, 'is_operand']));

        foreach($operands as $int){
           if ($total == 0 && $total = $int)
               continue;
           if ($operators[$inc]=='*') $total *= $int;
           if ($operators[$inc] == '/') $total /= $int;

           $inc++;
        }
        return $total;
    }

    protected function is_operand($x)
    {
        return $x == '*' || $x == '/' ? true : false;
    }

}

